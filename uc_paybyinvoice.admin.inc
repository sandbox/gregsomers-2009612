<?php

/**
 * @file
 * Payment pack administration menu items.
 *
 */

/**
 * Record invoice payment for an order.
 */
function uc_paybyinvoice_receive_invoice_payment_form($form, $form_state, $order) {
  $balance = uc_payment_balance($order);
  $form['balance'] = array('#markup' => uc_currency_format($balance));
  $form['order_id'] = array(
    '#type' => 'hidden',
    '#value' => $order->order_id,
  );
  $form['amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Amount'),
    '#default_value' => uc_currency_format($balance, FALSE, FALSE, '.'),
    '#size' => 10,
    '#field_prefix' => variable_get('uc_sign_after_amount', FALSE) ? '' : variable_get('uc_currency_sign', '$'),
    '#field_suffix' => variable_get('uc_sign_after_amount', FALSE) ? variable_get('uc_currency_sign', '$') : '',
  );
  $form['comment'] = array(
    '#type' => 'textfield',
    '#title' => t('Receipt No. / Comment'),
    '#description' => t('Any notes about the Invoice Payment, such as payment method (Cash, Offine Credit, Cheque ).'),
    '#size' => 64,
    '#maxlength' => 256,
  );
  $form['payment'] = array(
    '#type' => 'fieldset',
    '#title' => t('Payment date'),
    '#collapsible' => FALSE,
  );
  $form['payment']['payment_month'] = uc_select_month(NULL, format_date(REQUEST_TIME, 'custom', 'n'));
  $form['payment']['payment_day'] = uc_select_day(NULL, format_date(REQUEST_TIME, 'custom', 'j'));
  $form['payment']['payment_year'] = uc_select_year(NULL, format_date(REQUEST_TIME, 'custom', 'Y'), format_date(REQUEST_TIME, 'custom', 'Y'), format_date(REQUEST_TIME, 'custom', 'Y') + 1);
  foreach (array('payment_month', 'payment_day', 'payment_year') as $key) {
    $form['payment'][$key]['#prefix'] = '<div style="float: left; margin-right: 1em;">';
    $form['payment'][$key]['#suffix'] = '</div>';
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Confirm Payment'),
  );

  return $form;
}

/**
 * Generate form for invoice payment form
 */
function theme_uc_paybyinvoice_receive_invoice_payment_form($variables) {
  $form = $variables['form'];
  //drupal_add_js(drupal_get_path('module', 'uc_payment') . '/uc_payment.js');

  $output = '<p>' . t('Use the form to enter the payments into the system.') . '</p>';
  $output .= '<p><strong>' . t('Order balance:') . '</strong> '
           . drupal_render($form['balance']) . '</p>';

  $output .= drupal_render_children($form);

  return $output;
}

/**
 * Validate invoice payment form
 */
function uc_paybyinvoice_receive_invoice_payment_form_validate($form, &$form_state) {
  if (!is_numeric($form_state['values']['amount'])) {
    form_set_error('amount', t('The amount must be a number.'));
  }
}

/**
 * Submit invoice payment form
 */
function uc_paybyinvoice_receive_invoice_payment_form_submit($form, &$form_state) {
  global $user;

  uc_payment_enter($form_state['values']['order_id'], 'paybyinvoice',
                  $form_state['values']['amount'], $user->uid, '', $form_state['values']['comment']);

  db_insert('uc_payment_byinvoice')
    ->fields(array(
      'order_id' => $form_state['values']['order_id'],
      'payment_date' => mktime(12, 0, 0, $form_state['values']['payment_month'], $form_state['values']['payment_day'], $form_state['values']['payment_year']),
    ))
    ->execute();

  drupal_set_message(t('Payment received on @date.', array(
    '@date' => date(variable_get('uc_date_format_default', 'm/d/Y'), mktime(12, 0, 0, $form_state['values']['payment_month'], $form_state['values']['payment_day'], $form_state['values']['payment_year'])),
  )));

  drupal_goto('admin/store/orders/' . $form_state['values']['order_id']);
}
